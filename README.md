# commoop-runner-java

Commoop-runner is a python/flask-component in the [commoop-project](https://git.uni-due.de/commoop).
Its task is to provide a [ReST-API](https://en.wikipedia.org/wiki/Representational_state_transfer), which receives a list of source- and test-files and executes it. It does return its result to commoop-runner, by whom commoop-runner-java is started, monitored and terminated.

## Technical documentation

Please see [commoop-runner-doc](https://git.uni-due.de/commoop/commoop-runner-doc) for the comprehensive technical documentation. This README-file acts as a cheat-sheet to the essence from a standpoint of development or deployment.

## Quickstart

These instructions will get you a copy of the project up and running on the same machine, commoop-runner is running on.

### Method 1: Build an image from sourcecode

Clone this directory with `$ git clone git@git.uni-due.de:commoop/commoop-runner-java.git` to a location of your choice.
The repository has multiple branches, for developing-purpose please switch to `develop`: `$ git checkout develop`. The branch `master` is reserved for releases, to be merged from `develop`.

Enter the cloned folder with `$ cd commoop-runner-java`. Build the sourcecode with the Dockerfile, i.e. `$ docker build -t commoop-runner-java .`. The tag you provide with `-t` is used by commoop-runner, to locate and start the image - you must provide it when calling commoop-runner's API.

Once the image has been build and tagged appropriately and stored on the host-machine, on which commoop-runner is running, nothing else has to be done. It is not necessary to use `docker run` to execute commoop-runner-java, as this is the task of commoop-runner.

### Method 2: Fetch from registry

Fetch the latest image from Gitlab's registry:

```
$ docker login git.uni-due.de:6666
$ docker pull git.uni-due.de:6666/commoop/commoop-runner-java
```

### Running the tests

Run the tests via the following command, after you have built the image as described below:
```
$ docker run -v /var/run/docker.sock:/var/run/docker.sock -e OPENPATCH_HMAC_SECRET='s3cr3t' -e COMMOOP_BASICAUTH_SECRET='s3cr3t' --net commoop-runner-net commoop-runner-java python3 -m unittest -f -v tests/api_v1.py
```

Each tests has a DocString and generates messages, describing what went wrong and why.

### Deployment

This project is deployed by Docker as described in the provided [Dockerfile](Dockerfile). In addition to the sourcecode in this repository, the dockerfile installs and configures nginx to serve commoop-runner via uWSGI in production.

To be started by commoop-runner, it is only necessary to place this image on the same host, and adress it in commoop-runner's API by its given tag.

## Built With

* [Flask](http://flask.pocoo.org/) - Awesome microframework for webdevelopment with Python
* [Docker](http://www.docker.com) - Containerizing applications for security and easy deployment.

## Authors

* **Liebers, Jonathan** - *Initial work and realization in WT 2017/18* - https://github.com/jliebers

## License

Please see the [LICENSE](LICENSE) file for details.
