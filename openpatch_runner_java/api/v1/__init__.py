from flask import Blueprint

# Define Blueprint
api = Blueprint(
    'api',
    __name__,
    template_folder='templates',
    static_folder='static'
)

# Import all components
# These import statements MUST be placed below the blueprint-definition
# Do not move.
from . import routes