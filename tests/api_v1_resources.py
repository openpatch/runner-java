# This file contains various Java-Sourcecodes as python variables for tests/api_v1.py

hello_world_src = """public class HelloWorld {
    private static String msg = "HelloWorld";

    public static void main (String[] args) {
        System.out.println(msg);
    }

    public String getMsg(){
        return msg;
    }
}"""

hello_world_test_ok_src = """
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HelloWorldTestOk {
    HelloWorld hw = new HelloWorld();

    @Test
    public void testPrintMessage() {
        assertEquals("HelloWorld", hw.getMsg()); // A successfull test
    }
}"""

hello_world_test_fail_src = """
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class HelloWorldTestFail {
    HelloWorld hw = new HelloWorld();

    @Test
    public void failTest() {
        assertEquals("HelloFAIL", hw.getMsg()); // A test which will fail
    }
}"""

junit_trace_ok1 = """JUnit version 4.12\n.\nTime: 0.009\n\nOK (1 test)\n\n"""

junit_trace_ok2 = """JUnit version 4.12\n.\nTime: 0.009\n\nOK (42 test)\n\n"""

junit_trace_ok3 = """JUnit version 4.12\n.\nTime: 0.009\n\nOK (9999 test)\n\n"""

junit_trace_fail = 'JUnit version 4.12\n..E.E.E\nTime: 0.011\nThere were 3 failures:\n1) failingTestOne' \
                    '(HelloWorldTest)\norg.junit.ComparisonFailure: This is the result message for my first failing' \
                    ' test. expected:<Hello[Universe]> but was:<Hello[World]>\n\tat org.junit.Assert.assertEquals' \
                    '(Assert.java:115)\n\tat HelloWorldTest.failingTestOne(HelloWorldTest.java:15)\n\tat' \
                    ' sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.' \
                    'NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.' \
                    'DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.' \
                    'reflect.Method.invoke(Method.java:498)\n\tat org.junit.runners.model.FrameworkMethod$1.' \
                    'runReflectiveCall(FrameworkMethod.java:50)\n\tat org.junit.internal.runners.model.' \
                    'ReflectiveCallable.run(ReflectiveCallable.java:12)\n\tat org.junit.runners.model.' \
                    'FrameworkMethod.invokeExplosively(FrameworkMethod.java:47)\n\tat org.junit.internal.runners.' \
                    'statements.InvokeMethod.evaluate(InvokeMethod.java:17)\n\tat org.junit.runners.ParentRunner.' \
                    'runLeaf(ParentRunner.java:325)\n\tat org.junit.runners.BlockJUnit4ClassRunner.' \
                    'runChild(BlockJUnit4ClassRunner.java:78)\n\tat org.junit.runners.BlockJUnit4ClassRunner.' \
                    'runChild(BlockJUnit4ClassRunner.java:57)\n\tat org.junit.runners.ParentRunner$3.' \
                    'run(ParentRunner.java:290)\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)' \
                    '\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n\tat org.junit.' \
                    'runners.ParentRunner.access$000(ParentRunner.java:58)\n\tat org.junit.runners.ParentRunner$2.' \
                    'evaluate(ParentRunner.java:268)\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)' \
                    '\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\n\tat org.junit.runners.Suite.runChild' \
                    '(Suite.java:27)\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\n\tat' \
                    ' org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\n\tat org.junit.runners.' \
                    'ParentRunner.runChildren(ParentRunner.java:288)\n\tat org.junit.runners.ParentRunner.' \
                    'access$000(ParentRunner.java:58)\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.' \
                    'java:268)\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\n\tat org.junit.' \
                    'runner.JUnitCore.run(JUnitCore.java:137)\n\tat org.junit.runner.JUnitCore.run(JUnitCore.' \
                    'java:115)\n\tat org.junit.runner.JUnitCore.runMain(JUnitCore.java:77)\n\tat org.junit.runner.' \
                    'JUnitCore.main(JUnitCore.java:36)\n2) failingTestTwo(HelloWorldTest)\norg.junit.' \
                    'ComparisonFailure: This is the result message for my second failing test. expected:' \
                    '<Hello[Cosmos]> but was:<Hello[World]>\n\tat org.junit.Assert.assertEquals(Assert.java' \
                    ':115)\n\tat HelloWorldTest.failingTestTwo(HelloWorldTest.java:20)\n\tat sun.reflect.' \
                    'NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.' \
                    'invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.' \
                    'invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.' \
                    'java:498)\n\tat org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.' \
                    'java:50)\n\tat org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.' \
                    'java:12)\n\tat org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.' \
                    'java:47)\n\tat org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.' \
                    'java:17)\n\tat org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:325)\n\tat org.junit.' \
                    'runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:78)\n\tat org.junit.' \
                    'runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:57)\n\tat org.junit.' \
                    'runners.ParentRunner$3.run(ParentRunner.java:290)\n\tat org.junit.runners.ParentRunner$1.' \
                    'schedule(ParentRunner.java:71)\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.' \
                    'java:288)\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\n\tat org.junit.' \
                    'runners.ParentRunner$2.evaluate(ParentRunner.java:268)\n\tat org.junit.runners.ParentRunner.' \
                    'run(ParentRunner.java:363)\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\n\tat org.' \
                    'junit.runners.Suite.runChild(Suite.java:27)\n\tat org.junit.runners.ParentRunner$3.run' \
                    '(ParentRunner.java:290)\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)' \
                    '\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n\tat org.junit.runners' \
                    '.ParentRunner.access$000(ParentRunner.java:58)\n\tat org.junit.runners.ParentRunner$2.' \
                    'evaluate(ParentRunner.java:268)\n\tat org.junit.runners.ParentRunner.run(ParentRunner.' \
                    'java:363)\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\n\tat org.junit.runner.' \
                    'JUnitCore.run(JUnitCore.java:115)\n\tat org.junit.runner.JUnitCore.runMain(JUnitCore.java:77)' \
                    '\n\tat org.junit.runner.JUnitCore.main(JUnitCore.java:36)\n3) failingTestThreeWithoutMsg' \
                    '(HelloWorldTest)\norg.junit.ComparisonFailure: expected:<Hello[Cosmos]> but was:<Hello[World]>' \
                    '\n\tat org.junit.Assert.assertEquals(Assert.java:115)\n\tat org.junit.Assert.assertEquals' \
                    '(Assert.java:144)\n\tat HelloWorldTest.failingTestThreeWithoutMsg(HelloWorldTest.java:25)' \
                    '\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.' \
                    'NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.' \
                    'DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.' \
                    'reflect.Method.invoke(Method.java:498)\n\tat org.junit.runners.model.FrameworkMethod$1.' \
                    'runReflectiveCall(FrameworkMethod.java:50)\n\tat org.junit.internal.runners.model.' \
                    'ReflectiveCallable.run(ReflectiveCallable.java:12)\n\tat org.junit.runners.model.' \
                    'FrameworkMethod.invokeExplosively(FrameworkMethod.java:47)\n\tat org.junit.internal.' \
                    'runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)\n\tat org.junit.runners.' \
                    'ParentRunner.runLeaf(ParentRunner.java:325)\n\tat org.junit.runners.BlockJUnit4ClassRunner.' \
                    'runChild(BlockJUnit4ClassRunner.java:78)\n\tat org.junit.runners.BlockJUnit4ClassRunner.' \
                    'runChild(BlockJUnit4ClassRunner.java:57)\n\tat org.junit.runners.ParentRunner$3.' \
                    'run(ParentRunner.java:290)\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.' \
                    'java:71)\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n\tat ' \
                    'org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\n\tat org.junit.runners.' \
                    'ParentRunner$2.evaluate(ParentRunner.java:268)\n\tat org.junit.runners.ParentRunner.run' \
                    '(ParentRunner.java:363)\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\n\tat ' \
                    'org.junit.runners.Suite.runChild(Suite.java:27)\n\tat org.junit.runners.ParentRunner$3.' \
                    'run(ParentRunner.java:290)\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.' \
                    'java:71)\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\n\tat ' \
                    'org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\n\tat org.junit.runners.' \
                    'ParentRunner$2.evaluate(ParentRunner.java:268)\n\tat org.junit.runners.ParentRunner.' \
                    'run(ParentRunner.java:363)\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\n\t' \
                    'at org.junit.runner.JUnitCore.run(JUnitCore.java:115)\n\tat org.junit.runner.JUnitCore.' \
                    'runMain(JUnitCore.java:77)\n\tat org.junit.runner.JUnitCore.main(JUnitCore.java:36)\n\n' \
                    'FAILURES!!!\nTests run: 4,  Failures: 3\n\n'
